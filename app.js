const express = require("express");
const http= require("http");
const path = require("path");

const app = express();
const server = http.createServer(app);
const router = require("./server/router.js")

app.use('/country-list/content/', express.static(path.join(__dirname, './dist/country-list')))
app.use('/country-list', router);

server.listen(8080, ()=>console.log(8080));